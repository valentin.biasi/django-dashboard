from django.db import models
from django.utils import timezone
from django.urls import reverse


class Categorie(models.Model):
    nom = models.CharField(default='Nope', max_length=30)

    def __str__(self):
        return self.nom

class Article(models.Model):
    titre = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, null=True)
    auteur = models.CharField(max_length=42)
    contenu = models.TextField(null=True)
    date = models.DateTimeField(default=timezone.now, 
                                verbose_name="Date de parution")
    categorie = models.ForeignKey('Categorie', on_delete=models.CASCADE, default=None)

    class Meta:
        verbose_name = "article"
        ordering = ['date']
    
    def __str__(self):
        return self.titre

    def get_url(self):
        return reverse('view_article', args=[self.pk])


class Contact(models.Model):
    sujet = models.CharField(max_length=100)
    message = models.TextField(null=True)
    mail = models.CharField(max_length=100)
    renvoi = models.BooleanField(default=False)
    date = models.DateTimeField(default=timezone.now, 
                                verbose_name="Date d'envoi'")


class Moteur(models.Model):
    nom = models.CharField(max_length=25)

    def __str__(self):
        return self.nom

class Voiture(models.Model):
    nom = models.CharField(max_length=25)
    moteur = models.OneToOneField(Moteur, on_delete=models.CASCADE)

    def __str__(self):
        return self.nom