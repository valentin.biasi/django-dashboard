from django.urls import path
from . import views


urlpatterns = [
    path('', views.home, name='home'),
    path('home/', views.home),
    path('article/', views.view_article),
    path('article/<int:id_article>', views.view_article, name='view_article'),
    path('articles/<int:year>/<int:month>', views.list_articles, name='view_articles'),
    path('articles/<int:year>', views.list_articles),
    path('addition/<int:nombre1>/<int:nombre2>/', views.addition),
    path('date/', views.date_actuelle),
    path('addition/<int:num1>/<int:num2>', views.addition),
    path('contact/', views.contact, name='contact'),
]