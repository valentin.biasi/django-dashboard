from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from django.core.mail import send_mail

from datetime import datetime
import random

from blog.forms import ContactForm
from blog.models import Article, Contact
from adminlte.views import render_dashboard



def home(request):
    """ Exemple de page simple """
    return render(request, 'blog/home.html')


def view_article(request, *args, **kwargs):
    """ 
    Vue qui affiche un article selon son identifiant (ou ID, ici un numéro)
    Son ID est le second paramètre de la fonction (pour rappel, le premier
    paramètre est TOUJOURS la requête de l'utilisateur)
    """
    id_article = kwargs.get('id_article', None)
    page_name = "Liste des articles"
    articles = Article.objects.all()

    try:
        my_article = Article.objects.get( pk=id_article )
        msg = my_article.contenu
    except:
        msg = 'ok'

    if id_article == None:
        return render_dashboard(request, 'blog/article.html', locals())

    elif id_article == 0:
        return redirect("https:/datact.io")

    elif id_article > 1000:
        raise Http404('Trop gros')

    else:
        ls_articles = [random.randint(1,100) for i in range(5)]
        return render_dashboard(request, 'blog/article.html', locals())



def list_articles(request, year, **kwargs):
    """ Liste des articles d'un mois précis. """
    page_name = "Liste des articles par date"

    # Filtre des articles
    month = kwargs.get('month', None)
    if month == None:
        articles = Article.objects.filter(date__year=year)
    else:
        articles = Article.objects.filter(date__year=year, date__month=month)

    return render_dashboard(request, 'blog/article.html', locals())


def date_actuelle(request):
    return render(request, 'blog/date.html', {'date': datetime.now()})


def addition(request, num1, num2):    
    total = num1 + num2

    # Retourne nombre1, nombre2 et la somme des deux au tpl
    return render(request, 'blog/addition.html', locals())


def contact(request):
    page_name = "Formulaire de contact"

    form = ContactForm(request.POST or None)

    if form.is_valid(): 

        A = Contact()
        A.sujet = form.cleaned_data['sujet']
        A.message = form.cleaned_data['message']
        A.mail = form.cleaned_data['mail']
        A.renvoi = form.cleaned_data['renvoi']
        A.save()

        # Mail de copie
        if A.renvoi:
            send_mail(
                'Copie de votre message',
                A.sujet+'\n\n'+A.message,
                'valentin@datact.io',
                [A.mail],
                fail_silently=False,
            )

        envoi = True

    return render_dashboard(request, 'blog/contact.html', locals())