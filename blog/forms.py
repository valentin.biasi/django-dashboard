from django import forms

class ContactForm(forms.Form):
    sujet = forms.CharField(label='Sujet', 
                            required= True,
                            max_length=100,
                            widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Votre sujet'}))

    message = forms.CharField(label='Message', 
                            widget=forms.Textarea(attrs={'class':'form-control', 'placeholder':'Votre sujet'}))

    # message = forms.CharField(widget=forms.Textarea)

    mail = forms.EmailField(label='Email', 
                                required= True,
                                widget=forms.EmailInput(attrs={'class':'form-control', 'placeholder':'Votre adresse e-mail'}))

    renvoi = forms.BooleanField(help_text="Cochez si vous souhaitez obtenir une copie du mail envoyé.",
                                label='',
                                required=False,
                                widget=forms.CheckboxInput(attrs={'class':'form-check-label'}))
