from django.contrib import admin
from django.utils.text import Truncator

from blog.models import Categorie, Article, Contact

@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    # fields = ('titre', 'auteur', 'categorie', 'contenu')
    list_display   = ('titre', 'auteur', 'categorie', 'date', 'apercu_contenu')
    list_filter    = ('auteur', 'categorie',)
    date_hierarchy = 'date'
    ordering       = ('date', )
    search_fields  = ('titre', 'contenu')
    prepopulated_fields = {'slug': ('titre', ), }

    # Configuration du formulaire d'édition
    fieldsets = (
        # Fieldset 1 : meta-info (titre, auteur…)
       ('Général', {
            'classes': ['collapse', ],
            'fields': ('titre', 'auteur', 'categorie', 'slug')
        }),
        # Fieldset 2 : contenu de l'article
        ('Contenu de l\'article', {
           'description': 'Le formulaire accepte les balises HTML. Utilisez-les à bon escient !',
           'fields': ('contenu', )
        }),
    )


    def apercu_contenu(self, article):

        return Truncator(article.contenu).chars(60, truncate='...')

    # En-tête de notre colonne
    apercu_contenu.short_description = 'Aperçu du contenu'


admin.site.register(Categorie)


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    list_display   = ('mail', 'sujet', 'renvoi', 'apercu_contenu', 'date')
    date_hierarchy = 'date'
    ordering       = ('date', )
    search_fields  = ('mail', 'sujet')

    
    def apercu_contenu(self, contact):

        return Truncator(contact.message).chars(60, truncate='...')

    apercu_contenu.short_description = 'Aperçu du contenu'