from django.db import models
from rest_framework import serializers


class Peak(models.Model):
    """
    Peak model: List of mountain peaks
    """
    name = models.CharField(max_length=50, unique=True)
    lon = models.FloatField()
    lat = models.FloatField()
    altitude = models.FloatField()

    def __str__(self):
        return self.name

class CountryWhitelist(models.Model):
    """
    CountryWhitelist model: List of countries allowed to access the service
    """
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name


class PeakSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Peak
        fields = ['name', 'lon', 'lat', 'altitude']