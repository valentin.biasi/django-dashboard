from django.shortcuts import render
from .models import Peak, PeakSerializer
from rest_framework import viewsets


class PeakViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Peak to be viewed or edited.
    """
    queryset = Peak.objects.all()
    serializer_class = PeakSerializer
