from django.contrib import admin
from .models import Peak, CountryWhitelist

@admin.register(Peak)
class PeakAdmin(admin.ModelAdmin):
    """
    Administration view of the Peak model
    """
    list_display = ('name', 'lon', 'lat', 'altitude', )
    ordering       = ('name', )
    search_fields  = ('name', )


@admin.register(CountryWhitelist)
class CountryWhitelistAdmin(admin.ModelAdmin):
    """
    Administration view of the CountryWhitelist model
    """
    list_display = ('name', )
    ordering       = ('name', )
    search_fields  = ('name', )