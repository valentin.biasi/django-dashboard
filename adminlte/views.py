from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from adminlte.models import Menu, Attribute
import json

def add_to_context(context, **kwargs):
    """ Add a list of keyword arguments to the variable context """
    for key, value in kwargs.items():
        context[key] = value


def render_dashboard(request, url_page, *args, **kwargs):
    """ Exemple de page non valide au niveau HTML pour que l'exemple soit concis """
    context = {}
    if len(args)>0: context = args[0]

    # Loads the attributes table as a dictionary {'key':'value'}
    attributes=Attribute.objects.to_dict()
    # Loads the menus default values 
    sidebar = Menu.objects.filter( menu_name="sidebar" )
    navbar = Menu.objects.filter( menu_name="navbar" )
    navbar_right = Menu.objects.filter( menu_name="navbar-right" )
    
    add_to_context(context, attributes=attributes, sidebar=sidebar, navbar=navbar, navbar_right=navbar_right)
    return render(request, url_page, context)



def index(request):
    return render_dashboard(request, 'adminlte/main-content.html')


