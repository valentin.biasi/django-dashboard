from django.db import models
from django.utils import timezone


class Attribute_manager(models.Manager):
    """
    Attribute manager
    - to_dict(): Transforms the attributes table as a dictionary {'key':'value'}
    """
    def to_dict(self):
        
        attributes={}
        items = super().get_queryset().values('name', 'value')
        for item in items:
            key = item['name']
            value = item['value']
            attributes[key] = value

        return attributes


class Attribute(models.Model):
    """
    Attribute model: List of miscellaneous parameters specified by the project
    Check fixtures/attributes.json for more details
    """
    name = models.CharField(max_length=50, unique=True)
    value = models.CharField(max_length=200, default="")
    objects = Attribute_manager()

    def __str__(self):
        return self.name


class Menu(models.Model):
    """
    Menu model: List of default configuration of menus in the project
    Check fixtures/menus.json for more details
    """
    name = models.CharField(max_length=50)
    link = models.CharField(max_length=200, default="")
    menu_name = models.CharField(max_length=50, null=True)
    
    has_children = models.BooleanField(default=False)
    parent = models.IntegerField(default=0)
    
    icon = models.CharField(max_length=30, blank=True)
    classes = models.CharField(max_length=200, blank=True)

    badge_display = models.BooleanField(default=False)
    # Badge Types : badge-primary, badge-secondary, badge-success, badge-info, badge-warning, badge-danger, badge-light, badge-dark
    badge_type = models.CharField(max_length=30, blank=True)
    badge_value = models.CharField(max_length=30, blank=True)

    def __str__(self):
        return self.name

    def save(self):
        if self.badge_type:
            self.badge_display = True
        super(Menu, self).save()
