from django.contrib import admin
from adminlte.models import Attribute, Menu

@admin.register(Attribute)
class AttributeAdmin(admin.ModelAdmin):
    """
    Administration view of the Attribute model
    """
    list_display = ('id', 'name', 'value')
    ordering       = ('name', )
    search_fields  = ('name', 'value')

    # Attribute edit form
    fieldsets = (
        # Fieldset 1 : General
       ('General', {
            'fields': ('name', 'value')
        }),
    )

@admin.register(Menu)
class MenuAdmin(admin.ModelAdmin):
    """
    Administration view of the Menu model
    """
    list_display = ('id', 'name', 'link', 'menu_name', 'has_children', 'parent', 'icon', 'classes', 'badge_display', 'badge_type', 'badge_value')
    list_filter    = ('menu_name', 'has_children', 'badge_display')
    ordering       = ('id', )
    search_fields  = ('name', 'menu_name')
    
    # Menu edit form
    fieldsets = (
        # Fieldset 1 : General
       ('General', {
            'fields': ('name', 'link', 'menu_name')
        }),
        # Fieldset 2 : Hierarchy
        ('Hierarchy', {
            'description': 'Only one level at the maximum',
            'fields': ('has_children', 'parent')
        }),
        # Fieldset 3 : Style
        ('Style', {
            'classes': ['collapse', ],
            'fields': ('icon', 'classes')
        }),
        # Fieldset 3 : Badge
        ('Badge', {
            'classes': ['collapse', ],
            'fields': ('badge_display', 'badge_type', 'badge_value')
        }),
    )
