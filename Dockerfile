FROM python:3

# The enviroment variable ensures that the python output is set straight
# to the terminal with out buffering it first
ENV PYTHONUNBUFFERED 1

# create root directory for our project in the container
RUN mkdir /mountain_peaks

# Set the working directory to /music_service
WORKDIR /mountain_peaks

# Copy the current directory contents into the container at /music_service
ADD . /mountain_peaks/

# Install any needed packages specified in requirements.txt
RUN pip install -r requirements.txt